/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda;

import java.util.Arrays;

/**
 *
 * @author mcomas
 */
public class Composition {
    /*
     * When a composition is created no zero or negative value is allowed
     */
    private double[] components;
    public int  length = 0;
    public String name = null;
    
    private Composition(){
    };
    
    private Composition(int size){
        components = new double[size];
        length = size;
    }
    
    private Composition(double[] components){
        this.components = new double[components.length];
        length = components.length;
        System.arraycopy(components, 0, this.components, 0, components.length);
    }
    
    public static Composition createComposition(double[] components) throws CoDaException{
        for(int i=0; i < components.length; i++){
            if( !(components[i] > 0)){
                throw new CoDaException("Not all components are positive");
            }
        }
        return new Composition(components);
    }
    public static Composition[] createCompositions(double[][] components) throws CoDaException{
        int l = components.length;
        Composition []coda = new Composition[l];
        for(int i = 0; i<l; i++){
            coda[i] = createComposition(components[i]);
        }
        return coda;
    }
    
//    private void set(int i, double value){ 
//        components[i] = value; 
//    }

    public Composition setName(String name){
        this.name = name;
        return this;
    }
    
    public String getName(String name){
        return(this.name);
    }
    
    private double get(int i){ 
        return components[i]; 
    }
    
    public double component(int i){ 
        return components[i]; 
    }
    
    public double[] arrayCopy(){
        double res[] = new double[components.length];
        System.arraycopy(components, 0, res, 0, components.length);
        return res;
    }
    
    public int size(){ 
        return components.length; 
    }
    
    public Composition closure(double c){
        double total = 0;
        double result[] = new double[components.length];
        for(int i=0; i< components.length;i++) total += components[i];
        for(int i=0; i< components.length;i++) result[i] = components[i] / total;
        return new Composition(result);
    }
    
    public Composition perturbate(Composition p){
        int m = p.size();
        if(components.length != m) return null;
        
        double result[] = new double[components.length];
        for(int i=0;i<m;i++) 
            result[i] =  components[i] * p.get(i);
        
        return new Composition(result);
    }
    
    public Composition power(double p){
        double result[] = new double[components.length];
        for(int i=0;i<components.length;i++)
            result[i] = Math.pow(components[i], p);
        
        return new Composition(result);
    }
/**
 *
 *  @return  ALR transformation of Composition
 */
    public double[] alr(){
        return Functions.alr(components);
    }

    /**
     * @param clr_base double[m-1][m]
     * @return
     */
    public double[] ilr(double[][] clr_base){
        return Functions.ilr(components, clr_base);
    }

    /**
     * @param partition double[m-1][m]
     * @return
     */
    public double[] ilr(){
        return Functions.ilr(components, 
                Functions.defaultPartition(components.length));
    }
    public double[] ilr(int[][] partition){
        return Functions.ilr(components, partition);
    }
    
/**
 *
 *  @return CLR transformation of matrix x
 */
    public double[] clr(){
        return Functions.clr(components);
    }

    @Override
    public String toString(){
        String output = "";
        if(name != null)
            output += name + ": ";
        output += Arrays.toString(components);
        //int m = components.length - 1;
        //for(int i=0;i<m;i++)
        //    output += components[i] + " ";
        //output += components[m];
        return output;
    }
    
    public static class CoDaException extends Exception{
        public CoDaException(String message){
            super(message);
        }
    }
    
}
