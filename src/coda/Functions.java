/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coda;

import java.util.Arrays;

/**
 *
 * @author marc
 */
public class Functions {
    // Private contructor to avoid class construction
    private Functions(){ }
    
    /*
     * ALR function
     * 
     */
    public static double[] alr(double x[]){
        int m = x.length;
        double[] alr = new double[m-1];
        for(int i=0; i<m-1; i++){
                alr[i] = Math.log(x[i]/x[m-1]);
        }
        return alr;
    }
    public static double[][] alr(double x[][]){
        double [][]alr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            alr[i] = alr(x[i]);
        }
        return alr;
    }
    public static double[] alr(Composition x){
        return x.alr();
    }
    public static double[][] alr(Composition x[]){
        double [][]alr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            alr[i] = x[i].alr();
        }
        return alr;
    }

    /*
     * CLR function
     * 
     */
    
    public static double[] clr(double x[]){
        int m = x.length;
        double[] clr = new double[m];
        double geometric_mean;
        geometric_mean = 0;
        for(int i=0; i<m; i++){
            geometric_mean += Math.log(x[i]);
        }
        geometric_mean = Math.exp(geometric_mean/m);
        for(int i=0; i<m; i++){
                clr[i] = Math.log(x[i]/geometric_mean);
        }
        return clr;
    }
    public static double[][] clr(double x[][]){
        double [][]clr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            clr[i] = clr(x[i]);
        }
        return clr;
    }
    public static double[] clr(Composition x){
        return x.clr();
    }
    public static double[][] clr(Composition x[]){
        double [][]clr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            clr[i] = x[i].clr();
        }
        return clr;
    }
    /*
     * ILR function from clr base
     * 
     */
    public static double[] ilr(double x[], double[][] clr_base){
        int m = x.length;
        double[] ilr = new double[m-1];
        double[] clr = clr(x);
        for(int i=0;i<m-1;i++){
            ilr[i] = 0;
            for(int j=0;j<m;j++){
                ilr[i] += clr[j] * clr_base[i][j];
            }
        }
        return ilr;
    }    
    public static double[] ilr(Composition x, double[][] clr_base){
        return x.ilr(clr_base);
    }
    public static double[][] ilr(double x[][], double clr_base[][]){
        double [][]ilr = new double[x.length][];
        
        for(int i=0;i<x.length;i++){
            ilr[i] = ilr(x[i], clr_base);
        }
        return ilr;
    }
    public static double[][] ilr(Composition x[], double clr_base[][]){
        double [][]ilr = new double[x.length][];
        
        for(int i=0;i<x.length;i++){
            ilr[i] = x[i].ilr(clr_base);
        }
        return ilr;
    }  
    /*
     * ILR function from bipartition
     */
    public static double[] ilr(double x[], int[][] partition){
        return ilr(x, partitionToBase(partition));
    }
    public static double[] ilr(Composition x, int[][] partition){
        return x.ilr(partition);
    }
    public static double[][] ilr(double x[][], int partition[][]){
        double [][]ilr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            ilr[i] = ilr(x[i], partition);
        }
        return ilr;
    } 
    public static double[][] ilr(Composition x[], int partition[][]){
        double [][]ilr = new double[x.length][];
        for(int i=0;i<x.length;i++){
            ilr[i] = x[i].ilr(partition);
        }
        return ilr;
    }   
    /*
     * Default ILR function
     * 
     */
    public static double[] ilr(double x[]){
        return ilr(x, 
                Functions.defaultPartition(x.length));
    }
    public static double[] ilr(Composition x){
        return x.ilr();
    }
    public static double[][] ilr(double x[][]){
        double [][]ilr = new double[x.length][];
        int [][]partition = defaultPartition(x[0].length);
        for(int i=0;i<x.length;i++){
            ilr[i] = ilr(x[i], partition);
        }
        return ilr;
    }
    public static double[][] ilr(Composition x[]){
        double [][]ilr = new double[x.length][];
        int [][]partition = defaultPartition(x[0].size());
        for(int i=0;i<x.length;i++){
            ilr[i] = x[i].ilr(partition);
        }
        return ilr;
    }
     /**
     *  @param  data     double[M][N] matrix consisting of N vectors of size M
     *
     *  @return center of data consisting of a double[M] vector
     */
    private static int fillPartition(int partition[][], int row, int left, int right){
        if(right - left <= 1)
            return 0;
        if(right - left == 2){
            for(int value: partition[row])
                value = 0;
            partition[row][left] = 1;
            partition[row][left+1] = -1;
            return 1;
        }
        int middle = left + (1 + right - left)/2;
        int next_row = row;
        for(int value: partition[row])
            value = 0;
        for(int i=left;i<middle;i++){
            partition[row][i] = 1;
        }
        for(int i=middle;i<right;i++){
            partition[row][i] = -1;
        }
        next_row++;
        next_row += fillPartition(partition, next_row, left, middle);
        next_row += fillPartition(partition, next_row, middle, right);
        return next_row-row;
    }
    /**
     *
     * @param partition
     * @return
     */
    public static double[][] partitionToBase(int partition[][]){
        int m = partition.length + 1;
        double[][] psi = new double[m-1][m];
        double r, s;
        double c;
        // Psi construction
        for(int i=0;i<m-1;i++){
            r = 0;
            s = 0;
            for(int j=0;j<m;j++){
                r += (partition[i][j] == 1 ? 1.0: 0);
                s += (partition[i][j] == -1 ? 1.0: 0);
            }
            c = Math.sqrt(r*s/(r+s));
            for(int j=0;j<m;j++)
                psi[i][j] = (partition[i][j] == 1? c/r :
                    (partition[i][j] == -1 ? -c/s : 0));
        }
        return psi;
    }
    public static int[][] defaultPartition(int n_components){
        int partition[][] = new int[n_components-1][n_components];       
        fillPartition(partition, 0, 0, n_components);
        return partition;
    }
    public static double[] alrInv(double []x){
        double a[] = new double[x.length + 1];
        for(int i=0;i<x.length;i++)
            a[i] = Math.exp(x[i]);
        a[x.length] = 1;
        return a;
    }
    public static double[][] alrInv(double [][]x){
        double [][]alrInv = new double[x.length][];
        for(int i=0;i<x.length;i++){
            alrInv[i] = alrInv(x[i]);
        }
        return alrInv;
    }
    public static double[] clrInv(double []x){
        return null;
    }
    public static double[][] clrInv(double [][]x){
        return null;
    }
    public static double[] ilrInv(double []x){
        return null;
    }
    public static double[][] ilrInv(double [][]x){
        return null;
    }
    public static double[] ilrInv(double []x, int[][]partition){
        return null;
    }
    public static double[][] ilrInv(double [][]x, int[][] partition){
        return null;
    }
    public static double[] ilrInv(double []x, double[][] clr_base){
        return null;
    }
    public static double[][] ilrInv(double [][]x, double[][] clr_base){
        return null;
    }
    public static double[] center(double [][]x){
        double accum[] = new double[x[0].length];
        Arrays.fill(accum, 0);
        for(double v[]: x)
            for(int i = 0; i<x[0].length;i++)
                accum[i] += Math.log(v[i]);
        for(int i = 0; i<x[0].length;i++)
            accum[i] = Math.exp(accum[i] / x.length);
        return accum;
    }
    public static Composition center(Composition x[]){
        return null;
    }
    public static double[][] centerData(double [][]x){
        return null;
    }
    public static Composition[] centerData(Composition x[]){
        return null;
    }
    public static Composition[] perturbe(Composition a[], Composition b){
        return null;
    }
    public static Composition[] perturbe(Composition a[], Composition b[]){
        return null;
    }
    public static double[][] perturbe(double a[][], double b[]){
        return null;
    }
}
