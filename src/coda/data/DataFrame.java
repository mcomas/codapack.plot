/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.data;


import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * DataFrame class is the main object used for storing {@link Variable} classes in CoDaPack. 
 * DataFrame is extended from {@link HashMap} using variable names as main keys.
 * The main difference from {@link HashMap} is that DataFrame keeps the 
 * variables ordered.
 * 
 * @author marc
 */
public class DataFrame extends HashMap<String, Variable>{
    /**
     *
     */
    public static final long serialVersionUID = 1L;
    /**
     * Private variable:
     *  - name: DataFrame name.
     *  - varnames: Array containing the variable names ordered.
     */
    private String name;
    public final ArrayList<String> varnames = new ArrayList<>();
    /**
     * Default constructor. DataFrame is initialized with name "default"
     */
    public DataFrame(){
        this.name = "default";
    }
    /**
     * Constructor used to initialize DataFrame with a name
     *
     * @param name Constructor initializing the DataFrame name
     */
    public DataFrame(String name){
        this.name = name;
    }
    /**
     * Sets the name of this DataFrame
     *
     *  @param  name Set the name for the DataFrame
     *
     */
    public void setName(String name){
        this.name = name;
    }
    /**
     * Returns the name of this DataFrame
     *
     *  @return  name the name
     *
     */
    public String getName(){
    	return name;
    }
    public void rename(String oldname, String newname) throws DataFrameException{
        Variable var = this.get(oldname);
        if(this.containsKey(newname))
            throw new DataFrameException("Variable name already defined");
        
        var.setName(newname);
        super.put(newname, var);
        super.remove(oldname);
        int indexOf = varnames.indexOf(oldname);
        varnames.set(indexOf, newname);
        var.setName(newname);
        alertModification(var);
        
    }
    public Variable get(int index){
        return this.get(varnames.get(index));
    }
    /**
     * With this method, a variable is added to DataFrame. If the variable
     * name is already defined, the method add a character 'c' at the end
     * of the name.
     * 
     * @param variable
     * @return
     * @throws coda.data.DataFrame.DataFrameException
     */
    public Variable add(Variable variable) throws DataFrameException{
        if(varnames.contains(variable.name))
            throw new DataFrameException("Variable name already defined");
        int m = varnames.size();
        varnames.add(m, variable.name);
        Variable v = put(variable.name, variable);
        alertModification(v);
        return v;
    }
    public Variable replace(Variable variable) throws DataFrameException{
        if(!varnames.contains(variable.name))
            throw new DataFrameException("Variable does not exist");
        Variable v = put(variable.name, variable);
        alertModification(v);
        return v;
    }
    public Variable remove(String vname) throws DataFrameException{
        if(!varnames.contains(vname))
            throw new DataFrameException("Variable does not exist");
        varnames.remove(vname);
        Variable v = super.remove(vname);
        alertModification(v);
        return v;
    }
    public Variable remove(Variable variable) throws DataFrameException{
        return remove(variable.name);
    }
    public static class DataFrameException extends Exception{
        public DataFrameException(String message){
            super(message);
        }
    }
    /*
     * Delegating events
     */
    ArrayList<DataFrameListener> listeners = new ArrayList<>();
    public void addDataFrameListener(DataFrameListener listener){
        listeners.add(listener);
    }
    public void removeDataFrameListener(){
        listeners.clear();
    }
    public void alertModification(Variable v){
        for(DataFrameListener e: listeners){
            e.dataFrameModified(this);
        }
    }
    public interface DataFrameListener{
        public void dataFrameModified(DataFrame v);
    }
    
    /*
    * SELECTION FUNCTIONS
    */
    public double[][] select(String vars[]) throws DataFrameException{
        Variable var;
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        for(String v: vars){
            var = this.get(v);
            if(!var.isNumeric())
                throw new DataFrameException("Variable " + v + " is not numeric");
            int vsize = var.size();
            if(vsize < min) min = vsize;
            if(vsize > max) max = vsize;
        }
        if ( min != max)
            throw new DataFrameException("Different variable length min = " + min + " and max = " + max);
        
        double res[][] = new double[min][vars.length];
        for(int j = 0; j < vars.length; j++){
            var = this.get(vars[j]);
            for(int i = 0; i<min; i++){
                res[i][j] = ((Numeric) var.get(i)).getValue();
            }
        }
        return res;
    } 
    public HashMap<String, double[][]> select(String vars[], String category) throws DataFrameException{
        Variable var;
        Variable var_cat = this.get(category);
        int min = var_cat.size(), max = var_cat.size();
        for(String v: vars){
            var = this.get(v);
            if(!var.isNumeric())
                throw new DataFrameException("Variable " + v + " is not numeric");
            int vsize = var.size();
            if(vsize < min) min = vsize;
            if(vsize > max) max = vsize;
        }
        if ( min != max)
            throw new DataFrameException("Different variable length min = " + min + " and max = " + max);
        
        HashMap<String, Integer> count = new HashMap<>();
        for(Element el : var_cat){
            String lvl = el.toString();
            if( ! count.containsKey( lvl ) )
                count.put(lvl, 0);
            count.put(lvl, count.get(lvl) + 1);
        }
        
        HashMap<String, double[][]> res = new HashMap<>();
        for(String lvl : count.keySet()){
            res.put(lvl, new double[count.get(lvl)][vars.length]);
        }
        
        
            
        for(int i = 0; i<min; i++){
            String lvl = var_cat.get(i).toString();
            double r[][] = res.get(lvl);
            System.out.println(lvl);
            System.out.println(r.length - count.get(lvl));
            for(int j = 0; j < vars.length; j++){
                var = this.get(vars[j]);
                r[r.length - count.get(lvl)][j] = ((Numeric) var.get(i)).getValue();
            }
            count.put(lvl, count.get(lvl) - 1);
        }
        return res;
    } 
}
