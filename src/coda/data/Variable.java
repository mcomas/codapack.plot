/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coda.data;

import java.util.ArrayList;
/**
 *
 * @author marc
 */
public class Variable extends ArrayList<Element>{
    
    String name = null;
    private boolean factorized = false;
    
    public Variable(){}
    public Variable(String name){
        super();
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public Variable setName(String name){
        this.name = name;
        return this;
    }
    public boolean isFactorized(){
        return factorized;
    }
    public boolean isNumeric(){
        for(Element e: this)
            if(e instanceof Text)
                return false;
        return true;
    }
    public void factorized(boolean f){
        this.factorized = f;
        if(this.factorized){
            for(int i=0; i<this.size(); i++){
                Element el = this.get(i);
                if(el instanceof NonAvailable)
                    this.set(i, el);
                else if(el instanceof Numeric){
                    Numeric num = (Numeric)el;
                    double v = num.getValue();
                    if(v == (int)v){
                        this.set(i, new Text(new Integer((int)v).toString()));
                    }else{
                        this.set(i, new Text(new Float(v).toString()));
                    }
                    
                }else{
                    this.set(i, new Text( el.toString()) );
                }
            }
        }else{
            for(int i=0; i<this.size(); i++){
                Element el = this.get(i);
                if(el instanceof NonAvailable)
                    this.set(i, el);
                else
                    this.set(i, setElementFromString( this.get(i).toString()) );
            }
        }
    }
    public Element setElementFromString(String v) {
        if(this.factorized){
            return new Text(v).variable(this);
        }
        if("na".equals(v.toLowerCase())){
            return new NonAvailable().variable(this);
        }
        try{
            double vd = Double.parseDouble(v);
            if(vd == 0)
                return new Zero(0).variable(this);
            return new Numeric(vd).variable(this);
        }catch(NumberFormatException e){
            double dl = Zero.isZero(v);
            if(dl >= 0)
                return new Zero(dl).variable(this);
            else
                return new Text(v).variable(this);
        }
    }
    @Override
    public String toString(){
        String str_array = "";
        if(name != null){
            str_array = " " + name + "\n";
            int l = str_array.length();
            for(int i=0;i<l; i++){
                str_array += "=";
            }
            str_array += "\n";
        }
        for(Element el: this){
            str_array += el.toString() + "\n";
        }
        return str_array;
    }
}
