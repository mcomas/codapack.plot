/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coda.data.io;

import coda.data.DataFrame;
import coda.data.DataFrame.DataFrameException;
import coda.data.Variable;
import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author marc
 */
public class ExcelImporter implements Importer{
    String fname;
    int nsheet = 0;
    boolean header = true;
    
    final int first = 0;
    @Override
    public String getParameters() {
         String conf = "format:excel";
         conf += "¿" + fname;
         conf += "¿" + nsheet;
         conf += "¿" + (header?"T":"F");
         return conf;
    }
    @Override
    public ExcelImporter setParameters(String pars) {
        String parameters[] = pars.split("¿");
        if("format:excel".equals(parameters[0])) {
            fname = parameters[1];
            nsheet = Integer.parseInt(parameters[2]);
            header = ("T".equals(parameters[3]));
        }
        return this;
    }
    @Override
    public ExcelImporter setParameters(Component frame){
        JFileChooser chooseFile = new JFileChooser();
        chooseFile.setFileFilter(
                    new FileNameExtensionFilter("Excel files", "xls", "xlsx"));
        int sel = chooseFile.showOpenDialog(frame);
        File a = chooseFile.getSelectedFile();
        if(a == null || sel != JFileChooser.APPROVE_OPTION)
            return this;
        fname = chooseFile.getSelectedFile().getAbsolutePath();
        Workbook wb = null;
        try {
            InputStream inp = new FileInputStream(fname);
            if( fname.endsWith(".xls") ){
                wb = new HSSFWorkbook(inp);
            }else if( fname.endsWith(".xlsx") ){
                wb = new XSSFWorkbook(inp);
            }else{
                System.out.println ("Format not recognized.");
            }
            nsheet = selectSheet(frame, wb);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelImporter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelImporter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }

    public int selectSheet(Component frame, Workbook wb){
        if(wb.getNumberOfSheets() > 1){
            String name_sheet[] = new String[wb.getNumberOfSheets()];
            for(int i=0;i<wb.getNumberOfSheets();i++){
                name_sheet[i] = wb.getSheetName(i);
            }
            final JDialog dialog = new JDialog();
            final JList sheet_selection = new JList(name_sheet);
            final JScrollPane scroll = new JScrollPane();
            sheet_selection.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    if (evt.getClickCount() == 2){
                        dialog.dispose();
                    }
                }
            });
            dialog.setLocationRelativeTo(frame);
            dialog.setTitle("Choose one sheet");
            dialog.setModal(true);
            dialog.setSize(200,100);
            scroll.setViewportView(sheet_selection);
            dialog.getContentPane().add(scroll);
            dialog.setVisible(true);
            return sheet_selection.getSelectedIndex();
        }else{
            return 0;
        }
    }
    @Override
    public DataFrame importDataFrame() {
        Workbook wb = null;
        try {
            InputStream inp = new FileInputStream(fname);
            if( fname.endsWith(".xls") ){
                wb = new HSSFWorkbook(inp);
            }else if( fname.endsWith(".xlsx") ){
                wb = new XSSFWorkbook(inp);
            }else{
                System.out.println ("Format not recognized.");
            }    
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelImporter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelImporter.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        if(wb == null)
            return null;
        
        Sheet sheet = wb.getSheetAt(nsheet);
        ArrayList<Integer> columns = new ArrayList<Integer>();
        ArrayList<Variable> variables = new ArrayList<Variable>();
        Iterator<Cell> cells = sheet.getRow(first).cellIterator ();
        while(cells.hasNext()){
            if(header){
                Cell cell = cells.next();
                String name = null;
                if(cell.getCellType() == Cell.CELL_TYPE_STRING){
                    name = cell.getRichStringCellValue().getString().trim();
                }
                if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                    name = Double.valueOf(cell.getNumericCellValue()).toString();
                }
                if(name != null){
                    columns.add(cell.getColumnIndex());
                    variables.add(new Variable(name));
                }
            }else{
                Cell cell = cells.next();
                columns.add(cell.getColumnIndex());
                variables.add(new Variable("C" + Integer.toString(cell.getColumnIndex())));
            }
        }
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
        Iterator<Integer> it = columns.iterator();
        int k = 0;
        DataFrame df = new DataFrame();
        while(it.hasNext()){
            int ncol = it.next();
            Variable var = variables.get(k);

            int nrow = (header ? first + 1 : first);
            boolean cont = true;
            String str;
            while(cont){
                Row row = sheet.getRow(nrow);
                Cell cell = null;
                CellValue cellValue = evaluator.evaluate(cell);
                try{
                    cell = row.getCell(ncol);
                    cellValue = evaluator.evaluate(cell);
                    if(cellValue.getCellType() == Cell.CELL_TYPE_STRING){
                        String v = cellValue.getStringValue().trim();
                        var.add( var.setElementFromString(v));
                    }else if(cellValue.getCellType() == Cell.CELL_TYPE_NUMERIC){
                        String v = Double.toString(cellValue.getNumberValue());
                        var.add( var.setElementFromString(v));
                    }                    
                    
                }catch(NullPointerException ex){
                    cont = false;
                }
                nrow++;
            }
            try {
                df.add(var);
            } catch (DataFrameException ex) {
                Logger.getLogger(ExcelImporter.class.getName()).log(Level.SEVERE, null, ex);
            }
            k++;
        }
        return df;
    }
}
