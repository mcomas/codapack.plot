/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package coda.data.io;

import au.com.bytecode.opencsv.CSVReader;
import coda.data.DataFrame;
import coda.data.DataFrame.DataFrameException;
import coda.data.Element;
import coda.data.Numeric;
import coda.data.Variable;
import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author marc
 */
public class TextImporter implements Importer{
    String fname = null;
    public char separator = ';';
    public char decimal = '.';
    public boolean header = true;
    //public char quoteChar = '"';
    //public boolean doubleQuote = false;
    //CSVReader reader = null;
    @Override
    public String getParameters() {
         String conf = "format:text";
         conf += "¿" + fname;
         conf += "¿" + separator;
         conf += "¿" + decimal;
         conf += "¿" + (header?"T":"F");
         return conf;
    }
    //"format:text?fname?separator?decimal?header
    @Override
    public TextImporter setParameters(String pars) {
        String parameters[] = pars.split("¿");
        if("format:text".equals(parameters[0])) {
            fname = parameters[1];
            separator = parameters[2].charAt(0);
            decimal = parameters[3].charAt(0);
            header = ("T".equals(parameters[4]));
        }
        return this;
    }
       @Override
    public TextImporter setParameters(Component frame) {
        JFileChooser chooseFile = new JFileChooser();
        chooseFile.setFileFilter(
                    new FileNameExtensionFilter("CSV/text files", "csv", "txt"));
        int sel = chooseFile.showOpenDialog(frame);
        File a = chooseFile.getSelectedFile();
        if(a == null || sel != JFileChooser.APPROVE_OPTION)
            return this;
        fname = chooseFile.getSelectedFile().getAbsolutePath();
        boolean repeat = true;
        String sep = null;
        while(repeat){
            sep = JOptionPane.showInputDialog(frame, "Separator", separator);
            if(sep.length() == 1)
                repeat = false;
            else
                JOptionPane.showMessageDialog(frame, "Only one character");
        }
        if(sep != null)
            separator = sep.toCharArray()[0];
        
        repeat = true;
        String dec = null;
        while(repeat){
            dec = JOptionPane.showInputDialog(frame, "Decimal", decimal);
            if(".".equals(dec) || ",".equals(dec))
                repeat = false;
            else
                JOptionPane.showMessageDialog(frame, "Only '.' and ',' are available");
        }
        if(dec != null)
            decimal = dec.toCharArray()[0];
        
        return this;
    }
    @Override
    public DataFrame importDataFrame() {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(fname), separator);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TextImporter.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(reader == null)
            return null;
        DataFrame df = new DataFrame();
        try{
            String row[];
            ArrayList<Variable> variables = new ArrayList<>();
            if(header){
                if((row = reader.readNext()) != null){
                    for (String row1 : row) {
                        variables.add(new Variable(row1));
                    }
                }
            }else{
                if((row = reader.readNext()) != null){
                    for(int i = 0; i < row.length; i++){
                        Variable v = new Variable("V" + i);
                        variables.add(v);
                        
                        String str = row[i];
                        if(decimal == ','){
                            Element el = v.setElementFromString(str.replace(',', '.'));
                            if(el instanceof Numeric)
                                v.add(el);
                            else
                                v.add(v.setElementFromString(str));
                        }else{
                            v.add(v.setElementFromString(str));
                        }
                    }
                }
            }
            while((row = reader.readNext()) != null){
                for(int i = 0; i < row.length; i++){
                    Variable v = variables.get(i);
                    
                    String str = row[i];
                    if(decimal == ','){
                        Element el = v.setElementFromString(str.replace(',', '.'));
                        if(el instanceof Numeric)
                            v.add(el);
                        else
                            v.add(v.setElementFromString(str));
                    }else{
                        v.add(v.setElementFromString(str));
                    }
                }
            }
            for(Variable v: variables)
                df.add(v);
            
        }catch ( DataFrameException | IOException ex) {
            Logger.getLogger(TextImporter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return df;
    }
}