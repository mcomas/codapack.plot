package coda.gui.menu;


import coda.data.DataFrame;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 *
 * @author mcomas
 */
public class AbstractMenuDialog extends JDialog{
    final DataSelector ds;
    public JPanel optionsPanel = new JPanel();;
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    
    boolean allowEmpty = false;
    public AbstractMenuDialog(JFrame mainApp, String title, DataFrame df){
        super(  mainApp, title);
        ds = new DataSelector(df);
        initialize(mainApp);
    }

    private void initialize(JFrame jf){
//        Point p = mainApplication.getLocation();
//        p.x = p.x + (mainApplication.getWidth()-520)/2;
//        p.y = p.y + (mainApplication.getHeight()-430)/2;
        setLocation(jf.getLocation());

        setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        setSize(560,430);
        getContentPane().add(ds, BorderLayout.CENTER);

        JPanel eastPanel = new JPanel();
        optionsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Options"));
        optionsPanel.setPreferredSize(new Dimension(200,200));
        //optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
        //eastPanel.add(optionsPanel);
        getContentPane().add(optionsPanel, BorderLayout.EAST);

        JButton acceptButton = new JButton("Accept");
        southPanel.add(acceptButton);
        acceptButton.addActionListener(new java.awt.event.ActionListener() {
            
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //acceptButtonActionPerformed();
            }
        });
        JButton cancelButton = new JButton("Cancel");
        southPanel.add(cancelButton);
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
            }
        });
        getContentPane().add(southPanel, BorderLayout.SOUTH);
    }
    //public abstract void acceptButtonActionPerformed();

    public void closeMenuDialog(){        
        setVisible(false);
    }
    public DataSelector getDataSelector(){
        return ds;
    }
    public String[] getSelectedData(){
        return ds.getSelectedData();
    }
}


