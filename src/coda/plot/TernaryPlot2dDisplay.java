/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot;

import coda.plot.objects.Ternary2dObject;
import coda.plot.objects.Ternary2dGridObject;
import ext.triangle.Triangle2D;
import ext.triangle.Vertex2D;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author marc
 */
public class TernaryPlot2dDisplay extends coda.plot.CoDa2dDisplay{

    // Constructor parameters
    protected String textLabel[] = null;
    protected ArrayList<Ternary2dObject> visualObject = new ArrayList<>();
    
    protected boolean isCentered = false;
    protected double center[] = new double[3];
    
    // Simple vertices    
    public final double[] oriV1 = {0, 0.57735026918962576450914878050196};
    public final double[] oriV2 = {-0.5, -0.28867513459481288225457439025098};
    public final double[] oriV3 = {0.5, -0.28867513459481288225457439025098};
    private final double[][] V = new double[3][2];

    /*
     * Builder constructor for TernaryPlot2Display
     */
    public TernaryPlot2dDisplay(){ }
    
    public TernaryPlot2dDisplay(Ternary2dObject objects[]){
        super();
        visualObject.addAll(Arrays.asList(objects));        
    }
    public TernaryPlot2dDisplay(String names[]){
        super();
        // Builder parameters copy
        this.textLabel = names;
    }
    public TernaryPlot2dDisplay(String names[], Ternary2dObject objects[]){
        super();
        // Builder parameters copy
        this.textLabel = names;
        visualObject.addAll(Arrays.asList(objects));
    }
    public double[] getCenter(){
        return center;
    }
    public void setCenter(double center[]){
        this.center = center;
    }
    public void addCoDaObject(Ternary2dObject codaobject){
        visualObject.add(codaobject);
    }
    public void addCoDaObject(ArrayList<Ternary2dObject> codaobjects){
        for(Ternary2dObject codaobject: codaobjects)
            visualObject.add(codaobject);
    }
    public ArrayList<Ternary2dGridObject> getGrid(){
        ArrayList<Ternary2dGridObject> grids = new ArrayList<>();

        for(Ternary2dObject obj : visualObject){
            if((obj instanceof Ternary2dGridObject)){
                grids.add((Ternary2dGridObject)obj);
            }
        }
        return grids;
    }
    @Override
    public void transformData(){

        V[0] = transform(oriV1[0], oriV1[1], V[0]);
        V[1] = transform(oriV2[0], oriV2[1], V[1]);
        V[2] = transform(oriV3[0], oriV3[1], V[2]);
        if(! visualObject.isEmpty()){
            for(Ternary2dObject vo : visualObject){
                vo.transformObject(this);
            }
        }
    }
    @Override
    public void paintComponent(Graphics2D g2, double width, double height){
        super.paintComponent(g2, width, height);
        

        factor = 0.90 * (height < width ? height : width);
        defaultTransform = new AffineTransform(
                factor, 0 ,
                0, -factor,
                width/2, height/1.6);
        
        drawAreas(g2);    
        if(! visualObject.isEmpty()){
            for(Ternary2dObject vo : visualObject){
                vo.plotObject(g2);
            }
        }
        if(textLabel != null)
            drawLabels(g2);
        drawAxis(g2);
    }
    Color axis_color = Color.BLACK;
    Float axis_size = 1f;
    public void drawAxis(Graphics2D g2){
        g2.setColor( axis_color );
        g2.setStroke(new BasicStroke( axis_size,
                BasicStroke.JOIN_MITER,
                BasicStroke.CAP_ROUND));

        Point2D o1 = null, o2 = null, o3 = null;
        o1 = defaultTransform.transform(new Point2D.Double(V[0][0], V[0][1]), o1);
        o2 = defaultTransform.transform(new Point2D.Double(V[1][0], V[1][1]), o2);
        o3 = defaultTransform.transform(new Point2D.Double(V[2][0], V[2][1]), o3);
        g2.draw(new Line2D.Double(o1.getX(), o1.getY(), o2.getX(), o2.getY()));
        g2.draw(new Line2D.Double(o2.getX(), o2.getY(), o3.getX(), o3.getY()));
        g2.draw(new Line2D.Double(o3.getX(), o3.getY(), o1.getX(), o1.getY()));
    }
    Color area_color = Color.LIGHT_GRAY;
    public void drawAreas(Graphics2D g2){
        g2.setPaint( area_color );
        Point2D o1 = null, o2 = null, o3 = null;
        o1 = defaultTransform.transform(new Point2D.Double(V[0][0], V[0][1]), o1);
        o2 = defaultTransform.transform(new Point2D.Double(V[1][0], V[1][1]), o2);
        o3 = defaultTransform.transform(new Point2D.Double(V[2][0], V[2][1]), o3);
        Triangle2D border = new Triangle2D(
                new Vertex2D(o1),
                new Vertex2D(o2),
                new Vertex2D(o3));
        g2.fill(border);
    }
    public void drawLabels(Graphics2D g2){
        Font font = new Font("Monospace", Font.PLAIN, 15);
        g2.setFont(font);
        g2.setColor(Color.black);
        FontMetrics metric = g2.getFontMetrics();

        int separation = 10;
        double sep[][] = new double[3][2];
        for(int i=0;i<3;i++){
            sep[i][1] = V[i][1] - origin[1];
        }
        for(short i=0;i<3;i++){
            String text = isCentered ? "c(" + textLabel[i] +")" : textLabel[i];
            Point2D o = defaultTransform.transform(new Point2D.Double(V[i][0],V[i][1]), null);
            if(V[i][1] - origin[1] > 0)
                g2.drawString(text,
                    (int)o.getX() - metric.stringWidth(text)/2,
                    (int)o.getY() - 5 - separation );

            else
                g2.drawString(text,
                    (int)o.getX() - metric.stringWidth(text)/2,
                    (int)o.getY() + metric.getHeight() + separation);
    
        }
    }
    public void perturbate(double perturbation[]){
        for(Ternary2dObject object :visualObject){
            object.perturbeObject(perturbation);
        }
    }
        
    public static double[] ternaryTransform(double x, double y, double z){
        // Given a compositional vector (x,y,z) the function returns a
        // vector (u,v) representing the vector (x,y,z) in a simplex with
        // vertices (0, 0) (1, 0) (0.5, 0.8660254)
        double []res = new double[2];

        double k = x + y + z;
        //x = x/k; y = y/k; z = z/k;

        //k = x + y + z;
        res[0] = (0.5 * x + z) / k - 0.5;
        res[1] = 0.866025403784439 * x / k - 0.28867513459481288225457439025098;

        return res;
    }
    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        if(e.getKeyChar() == 'c'){
            if(!this.isCentered){
                double [] invCenter = new double[3];
                invCenter[0] = 1 / center[0];
                invCenter[1] = 1 / center[1];
                invCenter[2] = 1 / center[2];
                perturbate(invCenter);
                this.isCentered = true;
             }else{
                 double[] ones = {1, 1, 1};
                 perturbate(ones);
                 this.isCentered = false;
             }
             repaint();
        }
    }
}
