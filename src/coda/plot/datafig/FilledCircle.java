/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.datafig;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 *
 * @author marc
 */
public class FilledCircle implements CoDaShape{
    Color interior = Color.WHITE;
    Color contour = Color.BLACK;
    double size = 3f;
    public FilledCircle(){ }
    public FilledCircle(Color interior, Color contour, double size){
        this.interior = interior;
        this.contour = contour;
        this.size = size;
    }
    @Override
    public void plot(Graphics2D g2, Point2D p) {
        g2.setColor( interior );
        g2.fill( new Ellipse2D.Double(p.getX() - size, p.getY() - size, 2*size, 2*size) );
        g2.setColor( contour );
        g2.draw( new Ellipse2D.Double(p.getX() - size, p.getY() - size, 2*size, 2*size) );
    }
    @Override
    public Color getColor() {
        return interior;
    }
    @Override
    public void setColor(Color color) {
        interior = color;
    }
    @Override
    public void setSize(double size) {
        this.size = size;
    }
    @Override
    public double getSize() {
        return size;
    }

}
