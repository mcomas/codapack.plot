/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.datafig;

import ext.triangle.StarPolygon;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author marc
 */
public class FilledStar implements CoDaShape{
    Color interior = Color.WHITE;
    Color contour = Color.BLACK;
    double size = 3f;
    int vertexCount = 5;
    public FilledStar(int vertexCount){ };
    public FilledStar(int vertexCount, Color interior, Color contour, double size){
        this.interior = interior;
        this.contour = contour;
        this.size = size;

        this.vertexCount = vertexCount;
    }
    @Override
    public void plot(Graphics2D g2, Point2D p) {        
        StarPolygon star = new StarPolygon((int)p.getX(), (int)p.getY(), (int)(1.9*size),(int)(0.9*size), vertexCount, -Math.PI/2);

        g2.setColor( interior );
        g2.fill(star);
        g2.setColor( contour );
        g2.draw(star);
    }
    @Override
    public Color getColor() {
        return interior;
    }
    @Override
    public void setColor(Color color) {
        interior = color;
    }
    @Override
    public void setSize(double size) {
        this.size = size;
    }
    @Override
    public double getSize() {
        return size;
    }

}
