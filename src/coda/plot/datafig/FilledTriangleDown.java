/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.datafig;

import ext.triangle.Triangle2D;import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author marc
 */
public class FilledTriangleDown implements CoDaShape{
    Color interior = Color.WHITE;
    Color contour = Color.BLACK;
    double size = 3f;
    public FilledTriangleDown(){ }
    public FilledTriangleDown(Color interior, Color contour, double size){
        this.interior = interior;
        this.contour = contour;
        this.size = size;
    }
    @Override
    public void plot(Graphics2D g2, Point2D p) {
        double cx = p.getX();
        double cy = p.getY();

        double down = 2 * size * 0.28867513459481288225457439025098;
        Triangle2D tri = new Triangle2D(cx-size,cy-down,cx+size,cy-down,cx,cy+2*down);

        g2.setColor( interior );
        g2.fill(tri);
        g2.setColor( contour );
        g2.draw(tri);
    }
    @Override
    public Color getColor() {
        return interior;
    }
    @Override
    public void setColor(Color color) {
        interior = color;
    }
    @Override
    public void setSize(double size) {
        this.size = size;
    }
    @Override
    public double getSize() {
        return size;
    }

}
