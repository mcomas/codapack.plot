/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.objects;

//import coda.data.DataSet;
import coda.plot.datafig.FilledCircle;
import coda.plot.datafig.CoDaShape;
import coda.plot.CoDa2dDisplay;
import coda.plot.TernaryPlot2dDisplay;


//import ext.jama.Matrix;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 *
 * @author marc
 */
public final class Ternary2dDataObject implements Ternary2dObject{
    private double data[][];
    private double data0[][];
    private double dataset[][];
    private String data_labels[] = null;
    private CoDa2dDisplay display;
    private boolean showData[];
    double center[] = {1,1,1};
    private CoDaShape shape;
    public void commonIni(TernaryPlot2dDisplay display, double dataset[][]){
        this.display = display;
        this.dataset = dataset;
        this.shape = new FilledCircle();

        //center = Compositional.center(new Matrix(this.dataset).transpose().getArray());
        int n = this.dataset.length;
        data0 = new double[n][];
        data = new double[n][2];
        showData = new boolean[n];
        for(int i=0;i<n;i++){
            data0[i] = TernaryPlot2dDisplay.ternaryTransform(
                    this.dataset[i][0], this.dataset[i][1], this.dataset[i][2]);
        }
    }
    public Ternary2dDataObject(TernaryPlot2dDisplay display, double dataset[][]){
        commonIni(display, dataset);
    }
    public Ternary2dDataObject setShape(CoDaShape shape){
        this.shape = shape;
        return this;
    }
    public Ternary2dDataObject setLabels(String labels[]){
        this.data_labels = labels;
        return this;
    }
    @Override
    public Ternary2dDataObject setColor(Color color){
        shape.setColor(color);
        return this;
    }
    @Override
    public void plotObject(Graphics2D g2) {        
        g2.setStroke(new BasicStroke(1f ,
                BasicStroke.JOIN_MITER,
                BasicStroke.CAP_ROUND));
        Point2D o = null;
        for (double[] data1 : data) {
            o = display.getGeometry().transform(new Point2D.Double(data1[0], data1[1]), o);
            shape.plot(g2, o);
        }
    }
    @Override
    public void transformObject(coda.plot.CoDa2dDisplay display) {
        for(int i=0;i<data.length;i++){
                data[i] = display.transform(data0[i][0], data0[i][1], data[i]);
        }
    }

    @Override
    public void perturbeObject(double[] x) {
        for(int i=0;i<dataset.length;i++)
            data0[i] = TernaryPlot2dDisplay.ternaryTransform(
                    x[0] * dataset[i][0],
                    x[1] * dataset[i][1],
                    x[2] * dataset[i][2]);
    }

    @Override
    public void powerObject(double t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setVisible(boolean visible) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    public void showLabel(coda.plot.CoDa2dDisplay display, int X ,int Y){
        Point2D o = null;
            int minIndex = -1;
            double minValue = 10000;
            double x;
            double y;
            AffineTransform transform = display.getGeometry();
            for(int i=0;i<data.length;i++){
                o = transform.transform(
                        new Point2D.Double(data[i][0],data[i][1]), o);
                x = o.getX() - X;
                y = o.getY() - Y;
                if(x*x + y*y < minValue){
                    minIndex = i;
                    minValue = x*x + y*y;
                }
            }
            if(minValue < 15) showData[minIndex] = !showData[minIndex];
    }

    @Override
    public LegendItem getLegendItem() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double[] getCenter() {
        return center;
    }
        /**
     *
     * @param x
     * @param y
     * @param z
     * @return
     */

}
