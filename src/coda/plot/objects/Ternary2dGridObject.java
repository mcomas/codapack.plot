/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.objects;

import coda.plot.CoDa2dDisplay;
import coda.plot.TernaryPlot2dDisplay;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 *
 * @author marc
 */
public class Ternary2dGridObject implements Ternary2dObject{
    protected double[] definedGrid;

    private final double[][][] origGrid;
    private final double[][][] grid;
    private final CoDa2dDisplay display;
    Color color = Color.BLACK;
    float size = 1f;
    
    double center[] = {1,1,1};
    boolean isVisible = true;
 
    public Ternary2dGridObject(TernaryPlot2dDisplay display, double definedGrid[]){
        this.display = display;
        this.definedGrid = definedGrid;
        
        origGrid = new double[3*definedGrid.length][2][];
        grid = new double[3*definedGrid.length][2][2];
        setGrid(1,1,1);
    }

    public final void setGrid(double x, double y, double z){
        for(int i=0;i<definedGrid.length;i++){

            origGrid[3*i][0] = TernaryPlot2dDisplay.ternaryTransform(
                    x*definedGrid[i], y-y*definedGrid[i], 0);

            origGrid[3*i][1] = TernaryPlot2dDisplay.ternaryTransform(
                    x*definedGrid[i], 0, z-z*definedGrid[i]);

            origGrid[3*i+1][0] = TernaryPlot2dDisplay.ternaryTransform(
                    0, y*definedGrid[i], z-z*definedGrid[i]);

            origGrid[3*i+1][1] = TernaryPlot2dDisplay.ternaryTransform(
                    x-x*definedGrid[i], y*definedGrid[i], 0);

            origGrid[3*i+2][0] = TernaryPlot2dDisplay.ternaryTransform(
                    x-x*definedGrid[i], 0, z*definedGrid[i]);

            origGrid[3*i+2][1] = TernaryPlot2dDisplay.ternaryTransform(
                    0, y-y*definedGrid[i], z*definedGrid[i]);
        }
    }
    @Override
    public void plotObject(Graphics2D g2) {
        if(isVisible){
            g2.setColor( color );
            g2.setStroke(new BasicStroke( size,
                       BasicStroke.JOIN_MITER,
                       BasicStroke.CAP_ROUND));
            Point2D o1 = null, o2 = null, o3 = null;
            AffineTransform affine = display.getGeometry();
            for (double[][] grid1 : grid) {
                o1 = affine.transform(new Point2D.Double(grid1[0][0], grid1[0][1]), o1);
                o2 = affine.transform(new Point2D.Double(grid1[1][0], grid1[1][1]), o2);
                g2.draw( new Line2D.Double(o1.getX(), o1.getY(), o2.getX(), o2.getY()) );
            }
            Font font = new Font("Monospace", Font.PLAIN, 10);
            g2.setFont(font);
            g2.setColor( Color.black );
            FontMetrics metric = g2.getFontMetrics();

            double x[] = display.getVX();
            double y[] = display.getVY();
            double det = x[0] * y[1] - x[1] * y[0];

            int index;
            if(Math.abs(x[1]) < 0.001) index = 0;
            else if(x[0] / x[1] > 0) index = det > 0 ? 1 : 2;
            else index = det > 0 ? 2 : 1;

            double a = grid[index][1][0] - grid[index][0][0];
            double b = grid[index][1][1] - grid[index][0][1];
            double norm = Math.sqrt(a*a + b*b);
            double sep = a/norm;
            for(int j=0;j<definedGrid.length;j++){
                String val1 = Double.toString(definedGrid[j]);
                String val2 = Double.toString(definedGrid[definedGrid.length-j-1]);
                    o1 = affine.transform(new Point2D.Double(grid[3*j+index][0][0] - 0.05*sep, grid[3*j+index][0][1]), o1);
                    o2 = affine.transform(new Point2D.Double(grid[3*j+index][1][0] + 0.05*sep, grid[3*j+index][1][1]), o2);
                    g2.drawString(val1,
                        (int)o1.getX() - metric.stringWidth(val2)/2,
                        (int)o1.getY());
                    g2.drawString(val2,
                        (int)o2.getX() - metric.stringWidth(val2)/2,
                        (int)o2.getY());
            }
        }
    } 
    @Override
    public Ternary2dGridObject setColor(Color color) {
        this.color = color;
        return this;
    }
    @Override
    public void transformObject(coda.plot.CoDa2dDisplay display) {
        for(int i=0;i<grid.length;i++){
            grid[i][0] = display.transform(origGrid[i][0][0], origGrid[i][0][1], grid[i][0]);
            grid[i][1] = display.transform(origGrid[i][1][0], origGrid[i][1][1], grid[i][1]);
        }
    }
    @Override
    public void perturbeObject(double[] x) {
        center = x;
        setGrid(x[0], x[1], x[2]);
    }
    @Override
    public void powerObject(double t) {
       
    }
    @Override
    public void setVisible(boolean visible) {
        isVisible = visible;
    }
    @Override
    public LegendItem getLegendItem() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public double[] getCenter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
