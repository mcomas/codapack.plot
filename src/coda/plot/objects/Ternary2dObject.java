/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.objects;

/**
 *
 * @author marc
 */
public interface Ternary2dObject extends TernaryObject{

    void transformObject(coda.plot.CoDa2dDisplay display);

}
