/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package coda.plot.objects;


import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author mcomas
 */
public interface TernaryObject {
    TernaryObject setColor(Color color);

    void setVisible(boolean visible);

    void plotObject(Graphics2D g2);

    void perturbeObject(double x[]);

    void powerObject(double t);

    double[] getCenter();

    LegendItem getLegendItem();

    public class LegendItem{
        public short DOT = 0;
        public short LINE = 1;
        public String text;
        public String code;
        public short form;
        public LegendItem(String text, String code){
            this.text = text;
            this.code = code;
            this.form = DOT;
        }
        public LegendItem(String text, String code, short form){
            this.text = text;
            this.code = code;
            this.form = form;
        }
    }
}
