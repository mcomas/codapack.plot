/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import coda.Functions;
import coda.data.DataFrame;
import coda.data.io.ExcelImporter;
import coda.plot.TernaryPlot2dDisplay;
import coda.plot.objects.Ternary2dDataObject;
import coda.plot.objects.Ternary2dGridObject;
import java.awt.Color;
import java.util.HashMap;
import javax.swing.JFrame;

/**
 *
 * @author marc
 */
public class testDataFrameXLS {
    public static void main(String args[]) throws DataFrame.DataFrameException {
        String xlsFile = "format:excel¿/Users/marc/Dropbox/CompartidaCoDaWork/Course/CoDaCourse11/data/halimba.xls¿0¿T";
        ExcelImporter impXLS = new ExcelImporter().setParameters(xlsFile);
        DataFrame df = impXLS.importDataFrame();
        
        System.out.println( df.varnames );
        
        String sub[] = { "Al2O3", "SiO2", "Fe2O3"};

        TernaryPlot2dDisplay display = new TernaryPlot2dDisplay(sub);
        
        //display.addKeyListener(display);
        double definedGrid[] =
        {0.01, 0.05, 0.10, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99};
        display.addCoDaObject(new Ternary2dGridObject(display, definedGrid)
                .setColor(new Color(200, 70, 0)));
        
        double quarterGrid[] =
        {.25, 0.5, 0.75};
        display.addCoDaObject(new Ternary2dGridObject(display, quarterGrid)
                .setColor(Color.BLUE));
        HashMap<String, double[][]> dfs = df.select(sub, "Group");
        double a[][] = dfs.get("A");
        double b[][] = dfs.get("B");
        double c[] = a[0];
        display.addCoDaObject(new Ternary2dDataObject(display, a)
                .setColor(new Color(200, 70, 0)));
        display.addCoDaObject(new Ternary2dDataObject(display, b)
                .setColor(new Color(0, 70, 200)));
        
        display.setCenter(Functions.center(a));
        JFrame frame = new JFrame();
        frame.getContentPane().add(display);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
}