/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import coda.data.DataFrame;
import coda.data.io.ExcelImporter;
import coda.gui.menu.AbstractMenuDialog;
import javax.swing.JFrame;

/**
 *
 * @author marc
 */
public class testDataSelector {
    public static void main(String [] args) {
        String xlsFile = "format:excel¿/Users/marc/Dropbox/CompartidaCoDaWork/Course/CoDaCourse11/data/halimba.xls¿0¿T";
        ExcelImporter impXLS = new ExcelImporter().setParameters(xlsFile);
        DataFrame df = impXLS.importDataFrame();
        
        JFrame jf = new JFrame();
        AbstractMenuDialog amd = new AbstractMenuDialog(jf, "prova", df);
        amd.setVisible(true);
        jf.setVisible(true);
    }
}
