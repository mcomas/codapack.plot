/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import coda.data.DataFrame;
import coda.data.io.ExcelImporter;
import coda.gui.table.CoDaPackTable;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JFrame;

/**
 *
 * @author marc
 */
public class testImportXLS {
        public static void main(String [] args) throws FileNotFoundException, IOException, InterruptedException {

            JFrame frame1 = new JFrame();
            CoDaPackTable tab = new CoDaPackTable();
            frame1.getContentPane().add(tab);
            frame1.setSize(600,300);
            frame1.setLocation(30,30);
            frame1.setVisible(true);
            
            
            ExcelImporter imp = new ExcelImporter().setParameters(frame1);
//                    setWorkBook("/home/marc/codapack/arcticlake.xls");
            // Excel Importer imp = imp.createImporterMenu(frame1);
            //int nsheet = ;
            DataFrame df = imp.importDataFrame();
            tab.setDataFrame(df);
            
            
            
            
        }

}
