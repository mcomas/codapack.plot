/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

//import coda.stats.Compositional;
//import ext.jama.Matrix;
//import coda.plot.window.TernaryPlot2dWindow;
//import coda.plot.datafig.FilledCircle;
//import coda.plot.PlotUtils;
//
//import coda.plot.objects.Ternary2dObject;
//import coda.plot.objects.Ternary2dCurveObject;
//import coda.plot.objects.Ternary2dDataObject;
//import coda.plot.objects.Ternary2dGridObject;
import coda.plot.TernaryPlot2dDisplay;
import coda.plot.objects.Ternary2dCurveObject;
import coda.plot.objects.Ternary2dDataObject;
import coda.plot.objects.Ternary2dGridObject;
import coda.plot.objects.Ternary2dObject;
import java.awt.Color;
import javax.swing.JFrame;

/**
 *
 * @author mcomas
 */
public class testTernaryPlot01 {
    public static void main(String args[]) {

        String label_names[] = {"X", "Y", "Z"};
        
        System.out.println(data[0][0]);
        System.out.println(data[0][1]);

        TernaryPlot2dDisplay display = new TernaryPlot2dDisplay(label_names);
        
        double definedGrid[] =
        {0.01, 0.05, 0.10, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99};
        display.addCoDaObject(new Ternary2dGridObject(display, definedGrid)
                .setColor(new Color(200, 70, 0)));
        
        double quarterGrid[] =
        {.25, 0.5, 0.75};
        display.addCoDaObject(new Ternary2dGridObject(display, quarterGrid)
                .setColor(Color.BLUE));
        
        display.addCoDaObject(new Ternary2dDataObject(display, data)
                .setColor(new Color(200, 70, 0)));        
        
        double path[][] = new double[100][3];
        
        path[0][0] = path[0][1] = path[0][2] = 1;
        double step = 0.25;
        for(int i=1;i<100;i++){
            path[i][0] = path[i-1][0] * Math.pow(0.2, step);
            path[i][1] = path[i-1][1] * Math.pow(0.4, step);
            path[i][2] = path[i-1][2] * Math.pow(0.5, step);
        }
        
        display.addCoDaObject(new Ternary2dCurveObject(display, path)
                .setColor(new Color(200, 70, 0)));
        
        JFrame frame = new JFrame();
        frame.getContentPane().add(display);
        frame.setSize(400, 400);
        frame.setVisible(true);
//

//        
//
///*
//        CoDaObject curveObject1 = new CurveObject(display,
//                PlotUtils.segment(data[0], data[2], 200));
//        curveObject1.setColor(new Color(70, 200, 70));
//        display.addCoDaObject(curveObject1);
// */
///*
//        double o1[] = {2,1};
//        double a1[] = {2,1};
//        CoDaObject curveObject2 = new CurveObject(display,
//                PlotUtils.ellipse(o1, a1, 0, 1000));
//        curveObject2.setColor(new Color(200, 70, 70));
//        display.addCoDaObject(curveObject2);
//*/
//        double o2[] = {2,1};
//        double a2[] = new double[2];
//        for(int i=0;i<20;i++){
//            a2[0] = 0.15 * i;
//            a2[1] = 0.1 * i;
//            Ternary2dObject curveObject3 = new Ternary2dCurveObject(display,
//                    PlotUtils.ellipse(o2, a2, Math.PI, 200));
//            curveObject3.setColor(new Color(100, 70, 70));
//            display.addCoDaObject(curveObject3);
//        }
//        
//        double a[] = {1,1,1};
//        double v[] = {4,1.2,1};
//        Ternary2dObject lineObject1 = new Ternary2dCurveObject(display,
//                PlotUtils.line(a, v, 100));
//        lineObject1.setColor(Color.CYAN);
//        display.addCoDaObject(lineObject1);
//        
//        
//        Ternary2dObject dataObject = new Ternary2dDataObject(display, data).setShape(new FilledCircle());
//        dataObject.setColor(new Color(70, 70, 200));
//        display.addCoDaObject(dataObject);
//        
//        
//        //CoDaObject
//        TernaryPlot2dWindow frame = new TernaryPlot2dWindow(display, "Test");
//        frame.setCenter(Compositional.center(dataset1));
//        //JFrame frame = new JFrame();
//        //frame.setSize(600,500);
//        //frame.getContentPane().add(display);
//        frame.setVisible(true);
    }
    static double data[][] = {
            {31.7,3.8,6.4},
            {23.8,9,9.2},
            {9.100001,34.2,9.5},
            {23.8,7.2,10.1},
            {38.3,2.9,7.7},
            {26.2,4.2,12.5},
            {33,4.6,12.2},
            {5.2,42.9,9.600001},
            {11.7,26.7,9.600001},
            {46.6,0.7,5.6},
            {19.5,11.4,9.5},
            {37.3,2.7,5.5},
            {8.5,38.9,8},
            {12.9,23.4,15.8},
            {17.5,15.8,8.3},
            {7.3,40.9,12.9},
            {44.3,1,7.8},
            {32.3,3.1,8.7},
            {15.8,20.4,8.3},
            {11.5,23.8,11.6},
            {16.6,16.8,12},
            {25,6.8,10.9},
            {34,2.5,9.399999},
            {16.6,17.6,9.600001},
            {24.9,9.7,9.8}};
}