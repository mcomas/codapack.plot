/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;


import coda.data.DataFrame;
import coda.data.DataFrame.DataFrameException;
import coda.data.Text;
import coda.data.Numeric;
import coda.data.Variable;
import coda.data.Zero;

/**
 *
 * @author marc
 */
public class testingDataFrame {
    public static void main(String [] args) throws DataFrameException{
        Variable v = new Variable("V1");
        v.add(new Numeric(8));
        v.add(new Zero(34));
        v.add(new Numeric(7));
        v.add(new Text("Patata123"));
        System.out.println(v);
        
        new DataFrame().add(v);
        
    }
}
